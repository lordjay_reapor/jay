package com.example.foregroundlocation.jav.utils.location

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.example.foregroundlocation.R
import com.example.foregroundlocation.jav.LocationUpdatesService
import com.example.foregroundlocation.jav.LocationActivity
import com.example.foregroundlocation.jav.utils.SingletonHolder
import com.example.foregroundlocation.jav.utils.preferences.MapPreferences


class ForegroundNotif private constructor(context: Context) {

    private val context = context

    companion object : SingletonHolder<ForegroundNotif, Context>(::ForegroundNotif)

    val notification: Notification
        get() {

            val PACKAGE_NAME = "com.example.foregroundlocation"
            val EXTRA_STARTED_FROM_NOTIFICATION = "$PACKAGE_NAME.started_from_notification"
            val text = MapPreferences.getInstance(context).location_updates
            val intentMA = Intent(context, LocationActivity::class.java)
            val intentSE = Intent(context, LocationUpdatesService::class.java)
            intentSE.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true)

            val servicePendingIntent = PendingIntent.getService( // check how to cancel
                context,
                0,
                intentSE,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            val activityPendingIntent = PendingIntent.getActivity(
                context,
                0,
                intentMA,
                0
            )
            val builder = NotificationCompat.Builder(context)
                .addAction(
                    R.drawable.ic_launch,
                    "open",
                    activityPendingIntent)
                .addAction(
                    R.drawable.ic_cancel,
                    "close",
                    servicePendingIntent)
                .setContentTitle("Location service running")
                .setContentText("My Location: $text")
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("Location service running")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId("channel_01")
            }
            return builder.build()
        }

}