package com.example.foregroundlocation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.foregroundlocation.jav.LocationActivity
import com.example.foregroundlocation.jav.LocationFragment
import com.example.foregroundlocation.jav.utils.AddFragment.Companion.replaceFragment


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        LocationActivity.start(this)
        finish()

    }

}
