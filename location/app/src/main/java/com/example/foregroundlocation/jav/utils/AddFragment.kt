package com.example.foregroundlocation.jav.utils

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction


class AddFragment {

    companion object {

        fun AppCompatActivity.replaceFragment(fragment: Fragment, @IdRes containerViewId: Int) {

            val fm: FragmentManager? = supportFragmentManager
            val ft: FragmentTransaction = fm!!.beginTransaction()
            ft.replace(containerViewId, fragment)
            ft.commit()
        }
    }
}



