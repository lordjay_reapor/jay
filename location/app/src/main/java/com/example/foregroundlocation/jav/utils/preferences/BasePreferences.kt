package com.example.foregroundlocation.jav.utils.preferences

import android.content.Context

open class BasePreferences protected constructor(context: Context, name: String) {

    protected val name = name
    protected val pref = context.getSharedPreferences(name, Context.MODE_PRIVATE)

    protected fun saveBoolean(key: String,
                              value: Boolean) {
        val editor = pref.edit()
        editor.putBoolean(
            key,
            value
        )
        editor.apply()
    }

    protected fun saveString(key:String,
                             value: String?) {
        value?.let {
            val  editor = pref.edit()
            editor.putString(
                key,
                it
            )
            editor.apply()
        }
    }


}