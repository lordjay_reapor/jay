package com.example.foregroundlocation.jav.utils.preferences

import android.content.Context
import com.example.foregroundlocation.jav.utils.SingletonHolder

class MapPreferences private constructor(context: Context) : BasePreferences(context, "MapPreferences") {

    companion object : SingletonHolder<MapPreferences, Context>(::MapPreferences)

    enum class Key(val value: String) {
        REQUEST_LOCATION_UPDATES("requesting_locaction_updates"),
        LOCATION_UPDATES("location_updates")

    }

    var requesting_locaction_updates: Boolean
        get() {
            return pref.getBoolean(
                Key.REQUEST_LOCATION_UPDATES.value,
                false
            )
        }
        set(value) {
            saveBoolean(
                Key.REQUEST_LOCATION_UPDATES.value,
                value
            )
        }

    var location_updates: String?
        get() {
            return pref.getString(
                Key.LOCATION_UPDATES.value,
                null
            )
        }
        set(value) {
            saveString(
                Key.LOCATION_UPDATES.value,
                value
            )
        }


}