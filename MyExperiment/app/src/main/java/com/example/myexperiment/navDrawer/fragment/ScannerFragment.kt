package com.example.myexperiment.navDrawer.fragment


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Rect
import android.hardware.camera2.*
import android.media.ImageReader
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.Size
import android.view.*
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.myexperiment.R
import com.example.myexperiment.util.showMessage
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.fragment_incident_camera.*
import kotlinx.android.synthetic.main.fragment_scanner.*
import java.util.*
import kotlin.math.roundToInt
import kotlin.math.sqrt

class ScannerFragment : Fragment(){
    private lateinit var camerasource: CameraSource
    private lateinit var barcodeDetector: BarcodeDetector

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        barcodeDetector = BarcodeDetector.Builder(activity!!).setBarcodeFormats(Barcode.ALL_FORMATS).build()
        barcodeDetector.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {}
            override fun receiveDetections(detections: Detector.Detections<Barcode>) {
                val qrCodes = detections.detectedItems
                if (qrCodes.size() != 0) {
                    val qrCodesStr = qrCodes.valueAt(0).displayValue
                    Log.d("qweasd", "rec $qrCodesStr")

                    scanContent.post {
                        kotlin.run {
                            scanContent.text = qrCodes.valueAt(0).displayValue
                        }
                    }
                }
            }
        })

        camerasource = CameraSource.Builder(activity!!, barcodeDetector).setAutoFocusEnabled(true).build()
        camerapreview.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
                showMessage("Change", activity!!)

            }

            override fun surfaceCreated(holder: SurfaceHolder) {
                if (ActivityCompat.checkSelfPermission(
                        activity!!,
                        Manifest.permission.CAMERA
                    ) !== PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.CAMERA), 200)
                    return
                }
                camerasource.start(holder)
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                camerasource.stop()
            }
        })

    }





}
