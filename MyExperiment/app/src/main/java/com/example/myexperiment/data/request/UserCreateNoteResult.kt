package com.example.myexperiment.data.request

data class UserCreateNoteResult(
    val __abp: Boolean,
    val error: Any,
    val result: Boolean,
    val success: Boolean,
    val targetUrl: Any,
    val unAuthorizedRequest: Boolean
)