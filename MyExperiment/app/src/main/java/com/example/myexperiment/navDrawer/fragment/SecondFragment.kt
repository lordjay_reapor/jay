package com.example.myexperiment.navDrawer.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs

import com.example.myexperiment.R
import com.example.myexperiment.util.showMessage
import kotlinx.android.synthetic.main.fragment_second.*

class SecondFragment : Fragment() {
    private val args: SecondFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showMessage(args.firstString, activity!!)
        secondBtn.setOnClickListener {
            findNavController().navigate(R.id.action_secondFragment_to_dashboardFragment)
        }
    }
}
