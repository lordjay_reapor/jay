package com.example.myexperiment.data.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myexperiment.data.respond.NoteItem

@Database(entities = [(NoteItem::class)], version = 1)
abstract class AppDB : RoomDatabase() {
    abstract fun userNoteDao(): UserNoteDao
}