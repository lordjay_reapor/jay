package com.example.myexperiment.data.respond

data class UserCreateNote(
    val content: String
)