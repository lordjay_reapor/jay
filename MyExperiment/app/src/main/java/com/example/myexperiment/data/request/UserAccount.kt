package com.example.myexperiment.data.request

data class UserAccount(
    val `external`: External,
    val password: String,
    val userNameOrEmailAddress: String
)

data class External(
    val tenant: String
)
