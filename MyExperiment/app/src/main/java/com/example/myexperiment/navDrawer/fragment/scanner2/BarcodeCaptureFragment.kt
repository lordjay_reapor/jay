@file:Suppress("DEPRECATION")
package com.example.myapplication.scanner2


import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Camera
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

import com.example.myexperiment.R
import com.example.myexperiment.navDrawer.fragment.DashboardFragmentDirections
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_barcode_capture.scannerTorch
import java.io.IOException

class BarcodeCaptureFragment : Fragment(), View.OnTouchListener, BarcodeGraphicTracker2.BarcodeUpdateListener {

    private var mCameraSource: CameraSource2? = null
    private var mPreview: CameraSourcePreview2? = null
    private var mGraphicOverlay: GraphicOverlay2<BarcodeGraphic2>? = null

    private var scaleGestureDetector: ScaleGestureDetector? = null
    private var gestureDetector: GestureDetector? = null
    private var switchTorch = "off"

    override fun onBarcodeDetected(barcode: Barcode?) {
        val bar = barcode!!.displayValue
        val noContentAction = BarcodeCaptureFragmentDirections.actionBarcodeCaptureFragmentToOneFragment("QR/Barcode Content: $bar")
        findNavController().navigate(noContentAction)
        Log.d("zxczxc","Barcode read: $bar")
    }

    override fun onCreateView(  inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val v =  inflater.inflate(R.layout.fragment_barcode_capture, container, false)

        mPreview = v.findViewById(R.id.preview2)
        mGraphicOverlay =  v.findViewById<GraphicOverlay2<BarcodeGraphic2>>(R.id.graphicOverlay2)

        val rc = ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA)
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource()
        } else {
            requestCameraPermission()
        }
        gestureDetector = GestureDetector(activity, CaptureGestureListener())
        scaleGestureDetector = ScaleGestureDetector(activity, ScaleListener())
        v.setOnTouchListener(this)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scannerTorch.setOnClickListener {
            Toast.makeText(activity!!, "torch on", Toast.LENGTH_SHORT).show()
            switchTorch  = if(switchTorch == "off") "torch" else "off"
            scannerTorch.text = switchTorch
            mCameraSource!!.doTorch(switchTorch)
        }
    }


    private fun requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission")

        val permissions = arrayOf(Manifest.permission.CAMERA)

        if (!ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(activity!!, permissions, RC_HANDLE_CAMERA_PERM)
            return
        }


        val listener = View.OnClickListener {
            ActivityCompat.requestPermissions(activity!!, permissions,
                RC_HANDLE_CAMERA_PERM)
        }

        Snackbar.make(mGraphicOverlay!!, R.string.permission_camera_rationale,
            Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.ok, listener)
            .show()
    }

    @SuppressLint("InlinedApi")
    private fun createCameraSource() {
        val barcodeDetector = BarcodeDetector.Builder(activity).build()
        val barcodeFactory = BarcodeTrackerFactory2(mGraphicOverlay!!, activity!!)
        barcodeDetector.setProcessor(
            MultiProcessor.Builder(barcodeFactory).build()
        )

        if (!barcodeDetector.isOperational) {
            Log.w(TAG, "Detector dependencies are not yet available.")
        }

        var builder = CameraSource2.Builder(activity, barcodeDetector)
            .setFacing(CameraSource2.CAMERA_FACING_BACK)
            .setRequestedPreviewSize(1600, 1024)
            .setRequestedFps(1.0f)

        builder = builder
            .setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)

        mCameraSource = builder
            .build()
    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        if (mPreview != null) {
            mPreview!!.stop()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mPreview != null) {
            mPreview?.release()
        }
    }

    override fun onRequestPermissionsResult( requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: $requestCode")
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source")
            createCameraSource()
            return
        }
        Log.e(
            TAG, "Permission not granted: results len = " + grantResults.size +
                    " Result code = " + if (grantResults.isNotEmpty()) grantResults[0] else "(empty)"
        )

    }

    @Throws(SecurityException::class)
    private fun startCameraSource() {
        val code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
            activity!!
        )
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance().getErrorDialog(activity!!, code, RC_HANDLE_GMS)
            dlg.show()
        }

        if (mCameraSource != null) {
            try {
                mPreview!!.start(mCameraSource!!, mGraphicOverlay!!)
            } catch (e: IOException) {
                Log.e(TAG, "Unable to start camera source.", e)
                mCameraSource!!.release()
                mCameraSource = null
            }

        }
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {

        val b = scaleGestureDetector!!.onTouchEvent(motionEvent)

        val c = gestureDetector!!.onTouchEvent(motionEvent)

        return b || c || view.onTouchEvent(motionEvent)
    }

    // here
    private fun onTap(rawX: Float, rawY: Float): Boolean {
        // Find tap point in preview frame coordinates.
        val location = IntArray(2)
        mGraphicOverlay!!.getLocationOnScreen(location)
        val x = (rawX - location[0]) / mGraphicOverlay!!.widthScaleFactor
        val y = (rawY - location[1]) / mGraphicOverlay!!.heightScaleFactor

        // Find the barcode whose center is closest to the tapped point.
        var best: Barcode? = null
        var bestDistance = java.lang.Float.MAX_VALUE
        for (graphic in mGraphicOverlay!!.graphics) {
            val barcode = graphic.barcode
            if (barcode!!.boundingBox.contains(x.toInt(), y.toInt())) {
                // Exact hit, no need to keep looking.
                best = barcode
                break
            }
            val dx = x - barcode.boundingBox.centerX()
            val dy = y - barcode.boundingBox.centerY()
            val distance = dx * dx + dy * dy  // actually squared distance
            if (distance < bestDistance) {
                best = barcode
                bestDistance = distance
            }
        }

        if (best != null) {
            val asd = best.displayValue
            Toast.makeText(activity!!,"meron code $asd", Toast.LENGTH_SHORT).show()
            val data = Intent()
            data.putExtra(BarcodeObject, best)
            // setResult(CommonStatusCodes.SUCCESS, data)
            // finish()
            return false // here change to true
        }
        Toast.makeText(activity!!,"walang code", Toast.LENGTH_SHORT).show()
        return false
    }


    private inner class CaptureGestureListener : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            return onTap(e.rawX, e.rawY) || super.onSingleTapConfirmed(e)
        }
    }

    private inner class ScaleListener : ScaleGestureDetector.OnScaleGestureListener {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            mCameraSource!!.doZoom(detector.scaleFactor)
            return false
        }
        override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
            return true
        }
        override fun onScaleEnd(detector: ScaleGestureDetector) { }// swap to onScale
    }

    companion object {
        private const val TAG = "Barcode-reader"
        private const val RC_HANDLE_GMS = 9001    // intent request code to handle updating play services if needed.
        private const val RC_HANDLE_CAMERA_PERM = 2 // permission request codes need to be < 256
        const val BarcodeObject = "Barcode"
    }
}


