package com.example.myexperiment.data.respond

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class UserGetNoteResult(
    val __abp: Boolean,
    val error: Any,
    @SerializedName("result")
    // @Embedded(prefix = "col_name_")
    val result: NoteResult,
    val success: Boolean,
    val targetUrl: Any,
    val unAuthorizedRequest: Boolean
)

data class NoteResult(
    @SerializedName("items")
    val items: List<NoteItem>
)

@Entity(tableName = "note_list")
data class NoteItem(
    @ColumnInfo(name = "note_content")
    val content: String,
    @PrimaryKey
    val id: Int
)
