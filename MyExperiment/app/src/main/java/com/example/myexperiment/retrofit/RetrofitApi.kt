package com.example.myexperiment.retrofit

import com.example.myexperiment.data.request.UserAccount
import com.example.myexperiment.data.request.UserCreateNoteResult
import com.example.myexperiment.data.respond.AuthenticateResult
import com.example.myexperiment.data.respond.UserCreateNote
import com.example.myexperiment.data.respond.UserGetNoteResult
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface RetrofitApi {
    @POST("api/TokenAuth/Authenticate")
    fun authenticateUser(@Body post: UserAccount): Call<AuthenticateResult>

    @POST("api/services/app/Note/CreateNote")
    fun createNote(@Body post: UserCreateNote,
                   @Header("Authorization") authHeader: String): Call<UserCreateNoteResult>

    @GET("api/services/app/Note/GetNote")
    fun getNote(@Header("Authorization") authHeader: String): Call<UserGetNoteResult>
}