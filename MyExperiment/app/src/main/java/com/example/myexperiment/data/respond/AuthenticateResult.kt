package com.example.myexperiment.data.respond

import com.google.gson.annotations.SerializedName

data class AuthenticateResult(
    val __abp: Boolean,
    val error: Any,
    @SerializedName("result")
    val result: AuthResult,
    val success: Boolean,
    val targetUrl: Any,
    val unAuthorizedRequest: Boolean
)

data class AuthResult(
    val accessToken: String,
    val encryptedAccessToken: String,
    val expireInSeconds: Int,
    val passwordResetCode: Any,
    val refreshToken: String,
    val requiresTwoFactorVerification: Boolean,
    val returnUrl: Any,
    val shouldResetPassword: Boolean,
    val twoFactorAuthProviders: Any,
    val twoFactorRememberClientToken: Any,
    val userId: Int
)

