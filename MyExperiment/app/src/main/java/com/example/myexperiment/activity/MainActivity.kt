package com.example.myexperiment.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.room.Room
import com.example.myexperiment.R
import com.example.myexperiment.data.db.AppDB
import com.example.myexperiment.data.request.External
import com.example.myexperiment.data.request.UserAccount
import com.example.myexperiment.data.respond.AuthenticateResult
import com.example.myexperiment.data.respond.UserGetNoteResult
import com.example.myexperiment.navDrawer.DrawerActivity
import com.example.myexperiment.retrofit.RetrofitApi
import com.example.myexperiment.retrofit.RetrofitClient
import com.example.myexperiment.util.showMessage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_access.*
import kotlinx.android.synthetic.main.activity_main_log_in.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var loginPreferences: SharedPreferences
    private lateinit var sprintShipMenu: Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loginPreferences =  getSharedPreferences("userAccountPreferences", Context.MODE_PRIVATE)
        sprintShipMenu = Intent(this, DrawerActivity::class.java)

        checkLoginPreferences()

        signInBtn.setOnClickListener {
            var usernameString = username.text.toString()
            var passwordString = password.text.toString()
            authenticateUser(usernameString, passwordString)
        }
        logoImage.setOnLongClickListener {
            Toast.makeText(this, "click", Toast.LENGTH_SHORT).show()
            showAccess()
            true
        }
        testBtn.setOnClickListener {
            showDialog("TEST")
        }
        betaBtn.setOnClickListener {
            showDialog("BETA")
        }
        liveBtn.setOnClickListener {
            showLogIn()
        }

    }


    private fun checkLoginPreferences(){
        val checkUser: Boolean = loginPreferences.getBoolean("loggedIn", false)
        if(checkUser){
            val usernamePref: String? = loginPreferences.getString("username","defUN")
            val passwordPref: String? = loginPreferences.getString("password","defPW")
            if (usernamePref != null && passwordPref!= null) {
                authenticateUser(usernamePref, passwordPref)
            }
        }
    }

    private fun authenticateUser(usernameString: String, passwordString: String){
        signInBtn.isClickable = false
        preloadingMessage.text = "Checking user account.."
        val retrofitApi = RetrofitClient.retrofitApi
        val userExternal = External("Default")
        val userAccount = UserAccount(external = userExternal, userNameOrEmailAddress = usernameString, password = passwordString)


        startActivity(sprintShipMenu)
        finish()
        /*

         */
        val call = retrofitApi.authenticateUser(userAccount)
        call.enqueue(object : Callback<AuthenticateResult> {
            override fun onResponse(call: Call<AuthenticateResult>, response: Response<AuthenticateResult>) {
                if (!response.isSuccessful) {
                    preloadingMessage.text = " "
                    showMessage("No user found. Err: "+response.code(), this@MainActivity)
                    signInBtn.isClickable = true
                    return
                }
                else {
                    preloadingMessage.text = " "
                    val authenticateResult = response.body()
                    val success = authenticateResult!!.success
                    val tokenAuth = authenticateResult.result.accessToken // store this and use to request in retrofit

                    if (success) {
                        userAccountValid(usernameString, passwordString, tokenAuth)
                    } else {
                        preloadingMessage.text = " "
                        showMessage("Authenticate user timeout", this@MainActivity)
                        signInBtn.isClickable = true
                    }
                }
            }
            override fun onFailure(call: Call<AuthenticateResult>, t: Throwable) { }
        })
    }

    private fun userAccountValid(usernameString: String, passwordString: String, tokenAuth: String){
        val loginEditor: SharedPreferences.Editor = loginPreferences.edit()
        // loginEditor.clear()
        loginEditor.putBoolean("loggedIn", true)
        loginEditor.putString("username",usernameString)
        loginEditor.putString("password",passwordString)
        loginEditor.putString("authtoken",tokenAuth)
        loginEditor.apply()
        getData(tokenAuth)
    }

    private fun getData(tokenAuth: String) {
        preloadingMessage.text = "Downloading note list.."
        getNote(tokenAuth)

        // download driver and vehicle profile
        // download route
    }

    private fun getNote(tokenAuth: String) {
        val retrofitApi: RetrofitApi = RetrofitClient.retrofitApi
        val auth = "Bearer $tokenAuth"

        val call = retrofitApi.getNote(auth)
        call.enqueue(object : retrofit2.Callback<UserGetNoteResult> {
            override fun onResponse(call: Call<UserGetNoteResult>, response: Response<UserGetNoteResult>) {
                if(!response.isSuccessful){
                    return
                }
                val getNoteResultBody = response.body()
                val resultItem = getNoteResultBody!!.result.items

                val db = Room.databaseBuilder(applicationContext, AppDB::class.java, "sprintship-db").build()
                Thread{
                    db.userNoteDao().insertNote(resultItem)
                }.start()

                startActivity(sprintShipMenu)
                finish()
            }
            override fun onFailure(call: Call<UserGetNoteResult>, t: Throwable) { }
        })
    }

    private fun showLogIn(){
        userAccess.visibility = View.INVISIBLE
        userLogIn.visibility = View.VISIBLE
    }

    private fun showAccess(){
        userLogIn.visibility = View.INVISIBLE
        userAccess.visibility = View.VISIBLE
    }

    private fun showDialog(userAccess: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Warning")
        builder.setMessage("This will connect you to the $userAccess server. Do you still want to proceed?")

        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> showLogIn()
                DialogInterface.BUTTON_NEGATIVE -> showLogIn()
            }
        }

        builder.setPositiveButton("YES",dialogClickListener)
        builder.setNegativeButton("NO",dialogClickListener)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}
