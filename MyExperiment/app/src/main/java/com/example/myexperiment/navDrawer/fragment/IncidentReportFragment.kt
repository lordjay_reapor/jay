package com.example.myexperiment.navDrawer.fragment


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

import com.example.myexperiment.R
import com.example.myexperiment.data.request.UserCreateNoteResult
import com.example.myexperiment.data.respond.UserCreateNote
import com.example.myexperiment.retrofit.RetrofitApi
import com.example.myexperiment.retrofit.RetrofitClient
import com.example.myexperiment.util.showMessage
import kotlinx.android.synthetic.main.fragment_incident_report.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IncidentReportFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_incident_report, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val loginPreferences: SharedPreferences =  activity!!.getSharedPreferences("userAccountPreferences", Context.MODE_PRIVATE)
        val authtoken: String? = loginPreferences.getString("authtoken","defAT")

        incidentCamera.setOnClickListener {
            findNavController().navigate(R.id.action_incidentReportFragment_to_incidentCameraFragment)
        }

        incidenteBtn.setOnClickListener {
            if (authtoken != null)
                sendNote(authtoken)
        }

    }

    private fun sendNote(authtoken: String) {
        val incidentStr = incidenteTxt.text.toString()
        incidenteTxt.setText("")

        val retrofitApi: RetrofitApi = RetrofitClient.retrofitApi
        val auth = "Bearer $authtoken"
        val userCreateNote = UserCreateNote(incidentStr)

        showMessage( message = "Message sent", context = activity!!)
        val call = retrofitApi.createNote(userCreateNote, auth)
        call.enqueue(object : Callback<UserCreateNoteResult> {
            override fun onResponse(call: Call<UserCreateNoteResult>, response: Response<UserCreateNoteResult>) {
                if(!response.isSuccessful){
                    showMessage( message = "Message not sent. Err: "+response.code(), context = activity!!)
                    return
                }
            }
            override fun onFailure(call: Call<UserCreateNoteResult>, t: Throwable) { }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.incident_delete, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.action_incident_delete) {
            incidenteTxt.setText("")
        }
        return super.onOptionsItemSelected(item)
    }

}
