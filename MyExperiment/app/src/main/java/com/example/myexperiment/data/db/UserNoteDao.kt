package com.example.myexperiment.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.myexperiment.data.respond.NoteItem

@Dao
interface UserNoteDao {
    @Query("SELECT * FROM note_list")
    fun getNote(): List<NoteItem>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(resultEntry: List<NoteItem>)

    @Query("DELETE FROM note_list")
    fun deleteNote()

}