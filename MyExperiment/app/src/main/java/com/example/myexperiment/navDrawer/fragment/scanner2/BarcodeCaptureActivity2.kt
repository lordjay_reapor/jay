@file:Suppress("DEPRECATION")

package com.example.myapplication.scanner2

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.hardware.Camera
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.example.myexperiment.R
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.barcode_capture2.*
import java.io.IOException

class BarcodeCaptureActivity2 : AppCompatActivity(), BarcodeGraphicTracker2.BarcodeUpdateListener {
    override fun onBarcodeDetected(barcode: Barcode?) {
        val bar = barcode!!.displayValue
        Log.d("zxczxc","Barcode read: $bar")
    }

    private var mCameraSource: CameraSource2? = null
    private var mPreview: CameraSourcePreview2? = null
    private var mGraphicOverlay: GraphicOverlay2<BarcodeGraphic2>? = null

    private var scaleGestureDetector: ScaleGestureDetector? = null
    private var gestureDetector: GestureDetector? = null
    private var switchTorch = "off"

    public override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        setContentView(R.layout.barcode_capture2)

        mPreview = findViewById(R.id.preview)
        mGraphicOverlay = findViewById<GraphicOverlay2<BarcodeGraphic2>>(R.id.graphicOverlay)

        scannerTorch.setOnClickListener {
            Toast.makeText(this, "torch on", Toast.LENGTH_SHORT).show()

            switchTorch  = if(switchTorch == "off") "torch" else "off"
            scannerTorch.text = switchTorch
            mCameraSource!!.doTorch(switchTorch)
//                mPreview!!.stop()
//                createCameraSource(autoFocus, true)
        }

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        val rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource()
        } else {
            requestCameraPermission()
        }

        gestureDetector = GestureDetector(this, CaptureGestureListener())
        scaleGestureDetector = ScaleGestureDetector(this, ScaleListener())

        Snackbar.make(
            mGraphicOverlay!!, "Tap to capture. Pinch/Stretch to zoom",
            Snackbar.LENGTH_LONG
        )
            .show()
    }

    private fun requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission")

        val permissions = arrayOf(Manifest.permission.CAMERA)

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM)
            return
        }

        val thisActivity = this

        val listener = View.OnClickListener {
            ActivityCompat.requestPermissions(thisActivity, permissions,
                RC_HANDLE_CAMERA_PERM)
        }

        Snackbar.make(mGraphicOverlay!!, R.string.permission_camera_rationale,
            Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.ok, listener)
            .show()
    }

    override fun onTouchEvent(e: MotionEvent): Boolean {
        val b = scaleGestureDetector!!.onTouchEvent(e)

        val c = gestureDetector!!.onTouchEvent(e)

        return b || c || super.onTouchEvent(e)
    }

    @SuppressLint("InlinedApi", "ObsoleteSdkInt")
    private fun createCameraSource() {
        val context = applicationContext
        val barcodeDetector = BarcodeDetector.Builder(context).build()
        val barcodeFactory = BarcodeTrackerFactory2(mGraphicOverlay!!, this)
        barcodeDetector.setProcessor(
            MultiProcessor.Builder(barcodeFactory).build()
        )

        if (!barcodeDetector.isOperational) {
            Log.w(TAG, "Detector dependencies are not yet available.")
            val lowstorageFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val hasLowStorage = registerReceiver(null, lowstorageFilter) != null

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show()
                Log.w(TAG, getString(R.string.low_storage_error))
            }
        }

        var builder: CameraSource2.Builder = CameraSource2.Builder(applicationContext, barcodeDetector)
            .setFacing(CameraSource2.CAMERA_FACING_BACK)
            .setRequestedFps(15.0f)

        builder = builder
            .setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)

        mCameraSource = builder.build()
    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        if (mPreview != null) {
            mPreview!!.stop()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mPreview != null) {
            mPreview?.release()
        }
    }

    override fun onRequestPermissionsResult( requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: $requestCode")
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source")
            createCameraSource()
            return
        }
        Log.e(
            TAG, "Permission not granted: results len = " + grantResults.size +
                    " Result code = " + if (grantResults.isNotEmpty()) grantResults[0] else "(empty)"
        )

        val listener = DialogInterface.OnClickListener { _, _ -> finish() }

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Multitracker sample")
            .setMessage(R.string.no_camera_permission)
            .setPositiveButton(R.string.ok, listener)
            .show()
    }

    @Throws(SecurityException::class)
    private fun startCameraSource() {
        val code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
            applicationContext
        )
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS)
            dlg.show()
        }

        if (mCameraSource != null) {
            try {
                mPreview!!.start(mCameraSource!!, mGraphicOverlay!!)
            } catch (e: IOException) {
                Log.e(TAG, "Unable to start camera source.", e)
                mCameraSource!!.release()
                mCameraSource = null
            }

        }
    }

    // here
    private fun onTap(rawX: Float, rawY: Float): Boolean {
        // Find tap point in preview frame coordinates.
        val location = IntArray(2)
        mGraphicOverlay!!.getLocationOnScreen(location)
        val x = (rawX - location[0]) / mGraphicOverlay!!.widthScaleFactor
        val y = (rawY - location[1]) / mGraphicOverlay!!.heightScaleFactor

        // Find the barcode whose center is closest to the tapped point.
        var best: Barcode? = null
        var bestDistance = java.lang.Float.MAX_VALUE
        for (graphic in mGraphicOverlay!!.graphics) {
            val barcode = graphic.barcode
            if (barcode!!.boundingBox.contains(x.toInt(), y.toInt())) {
                // Exact hit, no need to keep looking.
                best = barcode
                break
            }
            val dx = x - barcode.boundingBox.centerX()
            val dy = y - barcode.boundingBox.centerY()
            val distance = dx * dx + dy * dy  // actually squared distance
            if (distance < bestDistance) {
                best = barcode
                bestDistance = distance
            }
        }

        if (best != null) {
            val asd = best.displayValue
            Toast.makeText(this,"meron code $asd", Toast.LENGTH_SHORT).show()
            val data = Intent()
            data.putExtra(BarcodeObject, best)
            setResult(CommonStatusCodes.SUCCESS, data)
            finish()
            return false // here change to true
        }
        Toast.makeText(this,"walang code", Toast.LENGTH_SHORT).show()
        return false
    }


    private inner class CaptureGestureListener : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            return onTap(e.rawX, e.rawY) || super.onSingleTapConfirmed(e)
        }
    }

    private inner class ScaleListener : ScaleGestureDetector.OnScaleGestureListener {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            mCameraSource!!.doZoom(detector.scaleFactor)
            return false
        }
        override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
            return true
        }
        override fun onScaleEnd(detector: ScaleGestureDetector) {
            // swap to onScale
        }
    }

    companion object {
        private const val TAG = "Barcode-reader"
        private const val RC_HANDLE_GMS = 9001    // intent request code to handle updating play services if needed.
        private const val RC_HANDLE_CAMERA_PERM = 2 // permission request codes need to be < 256
        const val AutoFocus = "AutoFocus" // constants used to pass extra data in the intent
        const val UseFlash = "UseFlash"
        const val BarcodeObject = "Barcode"
    }
}
