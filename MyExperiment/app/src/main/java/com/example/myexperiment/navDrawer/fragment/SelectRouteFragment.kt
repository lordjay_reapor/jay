package com.example.myexperiment.navDrawer.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.room.Room

import com.example.myexperiment.R
import com.example.myexperiment.data.db.AppDB
import kotlinx.android.synthetic.main.fragment_select_route.*


class SelectRouteFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_route, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewNoteFromDB()
    }

    private fun viewNoteFromDB() {
        val db = Room.databaseBuilder(activity!!.applicationContext, AppDB::class.java, "sprintship-db").build()

        Thread{
            val tableNote = db.userNoteDao().getNote()
            if(!tableNote.isEmpty()) {
                noTableFound.text = ""
                tableNote.forEach {
                    var content = ""
                    content += "ID: ${it.id} \n"
                    content += "Text: ${it.content} \n\n"
                    viewNote.append(content)
                }
            }
            else{
                noTableFound.text = "No table found"
            }
        }.start()
    }

}
