package com.example.myapplication.scanner2

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.example.myexperiment.R

import kotlinx.android.synthetic.main.activity_scanner.*

class ScannerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)

        read_barcode2.setOnClickListener {
            goToBarcode()
        }
    }

    private fun goToBarcode() {
        val intent = Intent(this, BarcodeCaptureActivity2::class.java)
        startActivityForResult(intent, 9001) // return result of qr code
    }
}
