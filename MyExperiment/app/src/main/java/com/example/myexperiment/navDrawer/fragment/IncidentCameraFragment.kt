package com.example.myexperiment.navDrawer.fragment


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.ImageFormat
import android.graphics.Rect
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.Image
import android.media.ImageReader
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.util.Size
import android.util.SparseIntArray
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat

import com.example.myexperiment.R
import com.example.myexperiment.util.showMessage
import kotlinx.android.synthetic.main.fragment_incident_camera.*
import java.io.*
import java.nio.ByteBuffer
import java.util.*
import kotlin.math.roundToInt
import kotlin.math.sqrt

class IncidentCameraFragment : Fragment() {

    private val ORIENTATIONS = SparseIntArray()
    init {
        ORIENTATIONS.append(Surface.ROTATION_0, 90)
        ORIENTATIONS.append(Surface.ROTATION_90, 0)
        ORIENTATIONS.append(Surface.ROTATION_180, 270)
        ORIENTATIONS.append(Surface.ROTATION_270, 180)
    }

    private lateinit var cameraId: String                   // cammera
    private lateinit var cameraDevice: CameraDevice
    private lateinit var cameraCaptureSessions: CameraCaptureSession
    private lateinit var captureRequestBuilder: CaptureRequest.Builder
    private lateinit var imageDimension: Size
    private lateinit var manager: CameraManager
    private lateinit var jpegSizes: Array<Size>
    private lateinit var imageReader: ImageReader
    private lateinit var reader: ImageReader


    private lateinit var file: File                         // file
    private val REQUEST_CAMERA_PERMISSION = 200
    private var mBackgroundHandler: Handler? = null
    private var mBackgroundThread: HandlerThread? = null

    private var flashLightStatus = false                    // torch

    private var fingerSpacing: Float = 0f                   // zoom
    private var zoomLevel: Float = 1f
    private var maximumZoomLevel: Float? = null
    private lateinit var rect: Rect
    private var zoom: Rect? = null

    var stateCallback: CameraDevice.StateCallback = object : CameraDevice.StateCallback(){
        override fun onOpened(@NonNull camera: CameraDevice) {
            cameraDevice = camera
            createCameraPreview()
        }
        override fun onDisconnected(@NonNull cameraDevice: CameraDevice) {
            cameraDevice.close()
        }
        override fun onError(@NonNull cameraDevice: CameraDevice, i: Int) {
            var cameraDevice = cameraDevice
            cameraDevice.close()
        }
    }

    private var textureListener: TextureView.SurfaceTextureListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(surfaceTexture: SurfaceTexture, i: Int, i1: Int) {
            openCamera()
        }

        override fun onSurfaceTextureSizeChanged(surfaceTexture: SurfaceTexture, i: Int, i1: Int) {

        }

        override fun onSurfaceTextureDestroyed(surfaceTexture: SurfaceTexture): Boolean {
            return false
        }

        override fun onSurfaceTextureUpdated(surfaceTexture: SurfaceTexture) {

        }
    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        return inflater.inflate(R.layout.fragment_incident_camera, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        assert(cameraView != null)
        cameraView.surfaceTextureListener = textureListener

        captureBtn.setOnClickListener {
            takePicture()
        }
        torchBtn.setOnClickListener {
            updatePreview(true, fingerSpacing)
        }
        cameraView.setOnTouchListener { view, motionEvent ->
            if(motionEvent.pointerCount == 2)
                updatePreview(false, currentFingerSpacing(motionEvent))
            true
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraDevice.close()
    }

    private fun switchButton(check: Boolean){
        if(check){
            captureBtn.visibility = View.INVISIBLE
            cameraDiscardBtn.visibility = View.VISIBLE
            cameraSaveBtn.visibility = View.VISIBLE
        } else{
            cameraDiscardBtn.visibility = View.INVISIBLE
            cameraSaveBtn.visibility = View.INVISIBLE
            captureBtn.visibility = View.VISIBLE
        }
    }

    private fun currentFingerSpacing(motionEvent: MotionEvent): Float {
        var x = motionEvent.getX(0) - motionEvent.getX(1)
        var y = motionEvent.getY(0) - motionEvent.getY(1)
        return sqrt(x * x + y * y)
    }

    private fun switchTorch(){
        if (flashLightStatus){
            captureRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_OFF)
            flashLightStatus = false
            torchBtn.setBackgroundResource(R.drawable.ic_default)
        }
        else{
            captureRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH)
            flashLightStatus = true
            torchBtn.setBackgroundResource(R.drawable.ic_default_yellow)
        }
    }

    private fun pinchZoom(levelOfZoom: Float){
        var delta: Float = 0.05f
        var currentFingerSpacing = levelOfZoom
        if(fingerSpacing != 0f){
            if (currentFingerSpacing > fingerSpacing) {
                if ((maximumZoomLevel!! - zoomLevel) <= delta) {
                    delta = maximumZoomLevel!! - zoomLevel
                }
                zoomLevel += delta
            } else if (currentFingerSpacing < fingerSpacing){
                if ((zoomLevel - delta) < 1f) {
                    delta = zoomLevel - 1f
                }
                zoomLevel -= delta
            }

            var ratio: Float = 1 / zoomLevel
            var croppedWidth: Int = rect.width() - (rect.width() * ratio).roundToInt()
            var croppedHeight: Int = rect.height() - (rect.height()* ratio).roundToInt()
            var l = croppedWidth/2
            var t = croppedHeight/2
            var r = rect.width() - croppedWidth/2
            var b = rect.height() - croppedHeight/2

            Log.d("testasd", "  \n left: $l \n top: $t \n right: $r \n botoom $b ")
            zoom = Rect(croppedWidth/2, croppedHeight/2,rect.width() - croppedWidth/2, rect.height() - croppedHeight/2)
            captureRequestBuilder.set(CaptureRequest.SCALER_CROP_REGION, zoom)

        }
        fingerSpacing = currentFingerSpacing
    }

    private fun openCamera() {
        manager = activity!!.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            cameraId = manager.cameraIdList[0]
            val characteristics = manager.getCameraCharacteristics(cameraId)
            val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!

            rect = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE)!!
            maximumZoomLevel = characteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM)!!
            imageDimension = map.getOutputSizes(SurfaceTexture::class.java)[0]
            jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!.getOutputSizes(    ImageFormat.JPEG)

            if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) !== PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(  activity!!, arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_CAMERA_PERMISSION )
                return
            }
            manager.openCamera(cameraId, stateCallback, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    private fun createCameraPreview() {
        torchBtn.visibility = View.VISIBLE
        switchButton(false)
        try {
            val texture = cameraView.surfaceTexture!!
            texture.setDefaultBufferSize(imageDimension.width, imageDimension.height)
            val surface = Surface(texture)

            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            captureRequestBuilder.addTarget(surface)
            cameraDevice.createCaptureSession(Arrays.asList(surface), object : CameraCaptureSession.StateCallback() {
                override fun onConfigured(@NonNull cameraCaptureSession: CameraCaptureSession) {
                    if (cameraDevice == null)
                        return
                    cameraCaptureSessions = cameraCaptureSession

                    if(flashLightStatus) {
                        flashLightStatus = false
                        updatePreview(true, fingerSpacing)
                    }
                    else
                        updatePreview(false, fingerSpacing)

                }

                override fun onConfigureFailed(@NonNull cameraCaptureSession: CameraCaptureSession) {
                    Toast.makeText(activity!!, "Changed", Toast.LENGTH_SHORT).show()
                }
            }, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    private fun updatePreview(checkTorch: Boolean, levelOfZoom: Float) {
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO)
        try {
            if(checkTorch) // open torch
                switchTorch()

            pinchZoom(levelOfZoom)

            val captureRequest = captureRequestBuilder.build()
            cameraCaptureSessions.setRepeatingRequest(captureRequest, null, mBackgroundHandler)


        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    private fun takePicture(){
        switchButton(true)
        try {
            var width = 640
            var height = 480
            if (jpegSizes != null && jpegSizes.size > 0) {
                width = jpegSizes[0].width
                height = jpegSizes[0].height
            }

            reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1)

            val outputSurface = ArrayList<Surface>(2)
            outputSurface.add(reader.surface)
            outputSurface.add(Surface(cameraView.surfaceTexture))

            val captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE)
            captureBuilder.addTarget(reader.surface)

            if (flashLightStatus) { // flash on take picture
                captureBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH)
                torchBtn.visibility = View.INVISIBLE
            }

            if (zoom != null) {
                captureBuilder.set(CaptureRequest.SCALER_CROP_REGION, zoom)
            }



            val rotation = activity!!.windowManager.defaultDisplay.rotation
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation))



            val captureListener = object : CameraCaptureSession.CaptureCallback() {
                override fun onCaptureCompleted(@NonNull session: CameraCaptureSession, @NonNull request: CaptureRequest, @NonNull result: TotalCaptureResult) {
                    super.onCaptureCompleted(session, request, result)
                }
            }

            cameraDevice.createCaptureSession(outputSurface, object : CameraCaptureSession.StateCallback() {
                override fun onConfigured(@NonNull cameraCaptureSession: CameraCaptureSession) {
                    try {
                        cameraCaptureSession.capture(captureBuilder.build(), captureListener, mBackgroundHandler)
                    } catch (e: CameraAccessException) {
                        e.printStackTrace()
                    }
                }
                override fun onConfigureFailed(@NonNull cameraCaptureSession: CameraCaptureSession) { }
            }, mBackgroundHandler)


            file = File(Environment.getExternalStorageDirectory().toString() + "/" + UUID.randomUUID().toString() + ".jpg")
            val readerListener = object : ImageReader.OnImageAvailableListener {
                override fun onImageAvailable(imageReader: ImageReader) {
                    var image: Image? = null
                    try {
                        image = reader.acquireLatestImage()
                        val buffer: ByteBuffer = image.planes[0].buffer
                        val bytes = ByteArray(buffer.capacity())
                        buffer.get(bytes)
                        save(bytes)

                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } finally {
                        run { image?.close() }
                    }
                }

                @Throws(IOException::class)
                private fun save(bytes: ByteArray) {
                    var outputStream: OutputStream? = null
                    try {
                        outputStream = FileOutputStream(file)
                        outputStream.write(bytes)
                    } finally {
                        outputStream?.close()
                    }
                }
            }
            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler)


            cameraSaveBtn.setOnClickListener {
                createCameraPreview()
            }

            cameraDiscardBtn.setOnClickListener {
                createCameraPreview()
            }

        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }


}
