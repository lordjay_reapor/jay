package com.example.myexperiment.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    private fun retrofit() : Retrofit = Retrofit.Builder()
        .baseUrl("https://sprinttek-api-beta.azurewebsites.net/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val retrofitApi: RetrofitApi = retrofit().create(RetrofitApi::class.java)
}