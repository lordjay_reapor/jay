package com.example.myexperiment.navDrawer.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.example.myexperiment.R
import kotlinx.android.synthetic.main.fragment_dashboard.*

class DashboardFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ori1.setOnClickListener {
            findNavController().navigate(R.id.action_dashboardFragment_to_firstFragment)
        }

        ori2.setOnClickListener {
            val noContentAction = DashboardFragmentDirections.actionDashboardFragmentToOneFragment("na")
            findNavController().navigate(noContentAction)
        }

        ori3.setOnClickListener {
            findNavController().navigate(R.id.action_dashboardFragment_to_scannerFragment)
        }


    }


}
